package main

import (
	"crypto/rand"
	"errors"
	"fmt"
	"log"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	"github.com/spf13/viper"
	"gorm.io/gorm"

	. "granola/models"
)

type Config struct {
	Secret       []byte
	DatabasePath string
	Admins       map[string]string
	Routing      struct {
		MinimumUsernameLength int
		MinimumPasswordLength int
	}
}

type RouteHandler struct {
	Translator ut.Translator
	DB         *gorm.DB
	Config     Config
}

type Server struct {
	Engine       *gin.Engine
	RouteHandler *RouteHandler
}

func (h RouteHandler) abortWithStatusAndText(c *gin.Context, status int, errorText string) {
	data := gin.H{
		"Config": h.Config.Routing,
		"Error":  errorText,
		"Status": status,
	}
	c.Negotiate(status, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "error",
		HTMLData: data,
		Data:     errorText,
	})
	c.Abort()
	log.Print(errorText)
}

func first(m map[string]string) string {
	for _, v := range m {
		return v
	}

	return ""
}

func (h RouteHandler) recovery(c *gin.Context, a any) {
	if err, ok := a.(validator.ValidationErrors); ok {
		translations := err.Translate(h.Translator)
		text := first(translations)
		h.abortWithStatusAndText(c, http.StatusBadRequest, text)
	} else if err, ok := a.(error); ok {
		switch {
		case errors.Is(err, gorm.ErrRecordNotFound):
			h.abortWithStatusAndText(c, http.StatusNotFound, "File not found!")
		default:
			h.abortWithStatusAndText(c, http.StatusBadRequest, err.Error())
		}
	} else {
		c.AbortWithStatus(http.StatusInternalServerError)
	}
}

func (h RouteHandler) show404(c *gin.Context) {
	h.abortWithStatusAndText(c, http.StatusNotFound, "File not found!")
}

func (h RouteHandler) showHome(c *gin.Context) {
	user, _ := c.Get("user")
	data := gin.H{
		"Config": h.Config.Routing,
		"Title":  "Home",
		"User":   user,
	}
	c.HTML(http.StatusOK, "home", data)
}

func (h RouteHandler) checkAuth(c *gin.Context) {
	session := sessions.Default(c)
	user := User{}
	result := h.DB.First(&user, session.Get("user"))
	if result.Error != nil {
		session.Delete("user")
		if err := session.Save(); err != nil {
			panic(err)
		}
	} else {
		c.Set("user", user)
	}
	c.Next()
}

func (h RouteHandler) requireAuth(c *gin.Context) {
	user, _ := c.Get("user")
	if user == nil {
		c.Redirect(http.StatusTemporaryRedirect, "/login")
		c.Abort()
		return
	}
	c.Next()
}

func (s Server) Run(addr string) {
	s.Engine.Run(addr)
}

func (s Server) Bootstrap(config Config) Server {
	r, t := configureGin(config.Secret, map[string]Validator{
		"username": {
			Func: func(fl validator.FieldLevel) bool {
				field, ok := fl.Field().Interface().(string)
				return ok && len(field) >= config.Routing.MinimumUsernameLength
			},
			Message: fmt.Sprintf("Username must be at least %d characters long", config.Routing.MinimumUsernameLength),
		},
		"password": {
			Func: func(fl validator.FieldLevel) bool {
				field, ok := fl.Field().Interface().(string)
				return ok && len(field) >= config.Routing.MinimumPasswordLength
			},
			Message: fmt.Sprintf("Password must be at least %d characters long", config.Routing.MinimumPasswordLength),
		},
	})
	db := openDatabase(config.DatabasePath)
	h := &RouteHandler{
		Translator: t,
		Config:     config,
		DB:         db,
	}

	r.Use(h.checkAuth)
	r.Use(gin.CustomRecovery(h.recovery))
	r.NoRoute(h.show404)

	h.registerUserRoutes(r)
	h.registerAdminRoutes(r, config.Admins)

	s.Engine = r
	s.RouteHandler = h

	return s
}

func secret() []byte {
	var secretBytes []byte
	if !viper.IsSet("cookie.secret") {
		log.Print("WARNING: cookie.secret not set, using random bytes. This means cookies won't persist between runs")
		secretBytes = make([]byte, 512)
		_, err := rand.Read(secretBytes)
		if err != nil {
			panic(err)
		}
	} else {
		secret := viper.GetString("cookie.secret")
		secretBytes = []byte(secret)
	}

	return secretBytes
}

func main() {
	viper.SetDefault("admins", map[string]string{})
	viper.SetDefault("MinimumUsernameLength", 3)
	viper.SetDefault("MinimumPasswordLength", 5)
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		log.Fatal(err)
	}

	config := Config{
		Secret:       secret(),
		DatabasePath: "database.db",
		Admins:       viper.GetStringMapString("admins"),
	}

	config.Routing.MinimumUsernameLength = viper.GetInt("MinimumUsernameLength")
	config.Routing.MinimumPasswordLength = viper.GetInt("MinimumPasswordLength")

	g := Server{}.Bootstrap(config)
	g.Run(":8080")
}
