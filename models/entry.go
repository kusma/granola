package models

import "fmt"

type Entry struct {
	ID      uint   `json:"id"`
	Title   string `json:"title"`
	Author  string `json:"author"`
	CompoID uint   `json:"compo"`
	UserID  uint   `json:"user"`
	Compo   Compo  `json:"-" binding:"-"` // Gin should ignore Compo; it should only be populated by Gorm.
	Locked  bool   `json:"locked"`
	// TODO: We should output a URI to the uploads for JSON.
	Uploads []Upload `json:"-"`
}

func (entry Entry) UserPath() string {
	return fmt.Sprintf("/entries/%d", entry.ID)
}

func (entry Entry) AdminPath() string {
	return fmt.Sprintf("/admin/entries/%d", entry.ID)
}
