package models

import (
	"strings"
	"fmt"
)

type Upload struct {
	ID          uint   `json:"id"`
	EntryID     uint   `json:"entry"`
	Entry       Entry  `json:"-" binding:"-"` // Gin should ignore Entry; it should only be populated by Gorm.
	Filename    string `json:"filename"`
	Content     []byte `json:"-"`
	ContentType string `json:"content_type"`
}

func (upload Upload) IsImage() bool {
	return strings.HasPrefix(upload.ContentType, "image/")
}

func (upload Upload) UserPath() string {
	return fmt.Sprintf("%v/uploads/%d", upload.Entry.UserPath(), upload.ID)
}

func (upload Upload) AdminPath() string {
	return fmt.Sprintf("%v/uploads/%d", upload.Entry.AdminPath(), upload.ID)
}
