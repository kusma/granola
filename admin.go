package main

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	. "granola/models"
)

func (h RouteHandler) showAdmin(c *gin.Context) {
	data := gin.H{
		"Config": h.Config.Routing,
		"Title":  "Admin",
	}
	c.HTML(http.StatusOK, "admin", data)
}

func (h RouteHandler) listInvites(c *gin.Context) {
	invites := []Invite{}
	result := h.DB.Preload("User").Find(&invites)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":  h.Config.Routing,
		"Invites": invites,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/invites",
		HTMLData: data,
		Data:     invites,
	})
}

func (h RouteHandler) newInvite(c *gin.Context) {
	invite := Invite{}
	if err := c.ShouldBind(&invite); err != nil {
		panic(err)
	}

	result := h.DB.Create(&invite)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/admin/invites")
	} else {
		c.JSON(http.StatusCreated, &invite)
	}
}

func (h RouteHandler) listUsers(c *gin.Context) {
	users := []User{}
	result := h.DB.Find(&users)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": h.Config.Routing,
		"Users":  users,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/users",
		HTMLData: data,
		Data:     users,
	})
}

func (h RouteHandler) createUser(c *gin.Context) {
	user := User{}
	if err := c.ShouldBind(&user); err != nil {
		panic(err)
	}

	result := h.DB.Create(&user)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/admin/users")
	} else {
		c.JSON(http.StatusCreated, &user)
	}
}

func (h RouteHandler) showUser(c *gin.Context) {
	user := User{}
	result := h.DB.First(&user, c.Param("user_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": h.Config.Routing,
		"User":   user,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/user",
		HTMLData: data,
		Data:     user,
	})
}

func (h RouteHandler) updateUser(c *gin.Context) {
	user := User{}
	result := h.DB.First(&user, c.Param("user_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	if err := c.ShouldBind(&user); err != nil {
		panic(err)
	}

	result = h.DB.Save(&user)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, user.AdminPath())
	} else {
		c.JSON(http.StatusOK, &user)
	}
}

func (h RouteHandler) deleteUser(c *gin.Context) {
	result := h.DB.Delete(&User{}, c.Param("user_id"))
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	c.Status(http.StatusNoContent)
}

func (h RouteHandler) listCompos(c *gin.Context) {
	compos := []Compo{}
	result := h.DB.Preload("Entries").Find(&compos)

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": h.Config.Routing,
		"Compos": compos,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/compos",
		HTMLData: data,
		Data:     compos,
	})
}

func (h RouteHandler) newCompo(c *gin.Context) {
	compo := Compo{}
	if err := c.ShouldBind(&compo); err != nil {
		panic(err)
	}

	result := h.DB.Create(&compo)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/admin/compos")
	} else {
		c.JSON(http.StatusCreated, &compo)
	}
}

func (h RouteHandler) showCompo(c *gin.Context) {
	compo := Compo{}
	result := h.DB.Preload("Entries.Uploads").First(&compo, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": h.Config.Routing,
		"Compo":  compo,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/compo",
		HTMLData: data,
		Data:     compo,
	})
}

func (h RouteHandler) updateCompo(c *gin.Context) {
	compo := Compo{}
	result := h.DB.First(&compo, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	if err := c.ShouldBind(&compo); err != nil {
		panic(err)
	}

	result = h.DB.Save(&compo)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, compo.AdminPath())
	} else {
		c.JSON(http.StatusOK, &compo)
	}
}

func (h RouteHandler) deleteCompo(c *gin.Context) {
	result := h.DB.Delete(&Compo{}, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "/admin/compos")
	} else {
		c.JSON(http.StatusOK, gin.H{})
	}
}

func (h RouteHandler) listEntries(c *gin.Context) {
	entries := []Entry{}
	result := h.DB.Preload("Uploads").Find(&entries)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	data := gin.H{
		"Config":  h.Config.Routing,
		"Entries": entries,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/entries",
		HTMLData: data,
		Data:     entries,
	})
}

func (h RouteHandler) newEntry(c *gin.Context) {
	args := struct {
		Title  string `json:"title" form:"title" binding:"required"`
		Author string `json:"author" form:"author" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	compo := Compo{}
	result := h.DB.First(&compo, c.Param("compo_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	entry := Entry{
		Title:  args.Title,
		Author: args.Author,
		Compo:  compo,
	}

	result = h.DB.Create(&entry)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, compo.AdminPath())
	} else {
		c.JSON(http.StatusCreated, &entry)
	}
}

func (h RouteHandler) showEntry(c *gin.Context) {
	entry := Entry{}
	result := h.DB.Preload("Uploads").Preload("Uploads.Entry").First(&entry, c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	compos := []Compo{}
	result = h.DB.Find(&compos)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": h.Config.Routing,
		"Entry":  entry,
		"Compos": compos,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/entry",
		HTMLData: data,
		Data:     entry,
	})
}

func (h RouteHandler) updateEntry(c *gin.Context) {
	args := struct {
		Title   string `json:"title" form:"title" binding:"required"`
		Author  string `json:"author" form:"author" binding:"required"`
		CompoID uint   `json:"compo" form:"compo" binding:"required"`
		Locked  *bool  `json:"locked" form:"locked" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	compo := Compo{}
	result := h.DB.First(&compo, args.CompoID)
	if result.Error != nil {
		panic(fmt.Errorf("compo %d not found", args.CompoID))
	}

	entry := Entry{}
	result = h.DB.Preload("Uploads").First(&entry, c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	entry.Title = args.Title
	entry.Author = args.Author
	entry.Compo = compo
	entry.Locked = *args.Locked

	result = h.DB.Save(&entry)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, entry.AdminPath())
	} else {
		c.JSON(http.StatusOK, &entry)
	}
}

func (h RouteHandler) deleteEntry(c *gin.Context) {
	result := h.DB.Delete(&Entry{}, c.Param("entry_id"))

	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.Status(http.StatusNoContent)
	}
}

func (h RouteHandler) listUploads(c *gin.Context) {
	uploads := []Upload{}
	result := h.DB.Find(&uploads)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	data := gin.H{
		"Config":  h.Config.Routing,
		"Uploads": uploads,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/uploads",
		HTMLData: data,
		Data:     uploads,
	})
}

func (h RouteHandler) newUpload(c *gin.Context) {
	upload := Upload{}
	if err := c.ShouldBind(&upload); err != nil {
		panic(err)
	}

	formFile, err := c.FormFile("file")
	if err != nil {
		panic(err)
	}
	file, err := formFile.Open()
	if err != nil {
		panic(err)
	}

	upload.Filename = formFile.Filename
	upload.Content, err = ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}
	upload.ContentType = http.DetectContentType(upload.Content)

	entry := Entry{}
	result := h.DB.Preload("Uploads.Entry").First(&entry, c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	upload.Entry = entry

	result = h.DB.Create(&upload)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, upload.AdminPath())
	} else {
		c.JSON(http.StatusCreated, &upload)
	}
}

func (h RouteHandler) showUpload(c *gin.Context) {
	upload := Upload{}
	result := h.DB.Omit("Content").
		Preload("Entry").
		Preload("Entry.Compo").
		Where("entry_id = ?", c.Param("entry_id")).
		First(&upload, c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": h.Config.Routing,
		"Upload": upload,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "admin/upload",
		HTMLData: data,
		Data:     upload,
	})
}

func (h RouteHandler) getUpload(c *gin.Context) {
	upload := Upload{}
	result := h.DB.Where("entry_id = ?", c.Param("entry_id")).First(&upload, c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}
	c.Header("Content-Disposition", "attachment; filename="+upload.Filename)
	c.Header("Content-Type", "application/octet-stream")
	c.Data(http.StatusOK, upload.ContentType, upload.Content)
}

func (h RouteHandler) deleteUpload(c *gin.Context) {
	result := h.DB.Where("entry_id = ?", c.Param("entry_id")).Delete(&Upload{}, c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.JSON(http.StatusOK, gin.H{})
	}
}

func (h RouteHandler) registerAdminRoutes(r *gin.Engine, admins map[string]string) {
	admin := r.Group("/admin", gin.BasicAuth(admins))
	admin.GET("", h.showAdmin)

	invites := admin.Group("/invites")
	invites.GET("", h.listInvites)
	invites.POST("", h.newInvite)

	users := admin.Group("/users")
	users.GET("", h.listUsers)
	users.POST("", h.createUser)

	user := users.Group("/:user_id")
	user.GET("", h.showUser)
	user.PUT("", h.updateUser)
	user.DELETE("", h.deleteUser)

	compos := admin.Group("/compos")
	compos.GET("", h.listCompos)
	compos.POST("", h.newCompo)

	compo := compos.Group("/:compo_id")
	compo.GET("", h.showCompo)
	compo.PUT("", h.updateCompo)
	compo.DELETE("", h.deleteCompo)
	compo.POST("/entries", h.newEntry)

	entries := admin.Group("/entries")
	entries.GET("", h.listEntries)

	entry := entries.Group("/:entry_id")
	entry.GET("", h.showEntry)
	entry.PUT("", h.updateEntry)
	entry.DELETE("", h.deleteEntry)

	uploads := entry.Group("/uploads")
	uploads.GET("", h.listUploads)
	uploads.POST("", h.newUpload)

	upload := uploads.Group("/:upload_id")
	upload.GET("", h.showUpload)
	upload.DELETE("", h.deleteUpload)
	upload.GET("/data", h.getUpload)
}
