package main

import (
	"bytes"
	"net/http"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func TestRoutes(t *testing.T) {
	r := setupTestRouter(t)

	code, _ := fetchAdminAPIEndpoint(r, "GET", "/admin/", nil)
	assert.Equal(t, http.StatusMovedPermanently, code)

	code, _ = fetchAdminAPIEndpoint(r, "GET", "/admin", nil)
	assert.Equal(t, http.StatusOK, code)
}

func TestComposEmpty(t *testing.T) {
	r := setupTestRouter(t)

	code, _ := fetchAdminAPIEndpoint(r, "GET", "/admin/compos/", nil)
	assert.Equal(t, http.StatusMovedPermanently, code)

	code, data := fetchAdminAPIEndpoint(r, "GET", "/admin/compos", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, "[]", data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/compos/1", nil)
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/compos/18446744073709551615", nil)
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/compos/340282366920938463463374607431768211455", nil)
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)
}

func TestCreateCompo(t *testing.T) {
	r := setupTestRouter(t)

	code, data := fetchAdminAPIEndpoint(r, "GET", "/admin/compos", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[]`, data)

	code, data = createCompo(r, "test compo", false)
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id":1, "name":"test compo", "locked":false}`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/compos", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[{"id":1, "name":"test compo", "locked":false}]`, data)

	code, _ = fetchAdminAPIEndpoint(r, "GET", "/admin/compos/1/", nil)
	assert.Equal(t, http.StatusMovedPermanently, code)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/compos/1", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{"id":1, "name":"test compo", "locked":false}`, data)

	code, data = fetchAdminAPIEndpoint(r, "DELETE", "/admin/compos/1", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{}`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/compos", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[]`, data)
}

func TestUpdateCompo(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)

	code, data := fetchAdminAPIEndpoint(r, "GET", "/admin/compos", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[{"id":1, "name":"test compo", "locked":false}]`, data)

	code, data = updateCompo(r, 1, gin.H{"name": "updated test compo", "locked": true})
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{"id":1, "name":"updated test compo", "locked":true}`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/compos", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[{"id":1, "name":"updated test compo", "locked":true}]`, data)
}

func TestCreateEntry(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)

	code, data := fetchAdminAPIEndpoint(r, "GET", "/admin/entries", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[]`, data)

	code, data = createEntry(r, 1, "test-entry", "test-author")
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id":1, "compo":1, "author":"test-author", "title":"test-entry", "user":0, "locked":false}`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/entries", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[{"id":1, "compo":1, "author":"test-author", "title":"test-entry", "user":0, "locked":false}]`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/entries/1", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{"id":1, "compo":1, "author":"test-author", "title":"test-entry", "user":0, "locked":false}`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/entries/2", nil)
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)

	code, data = createEntry(r, 2, "test-entry", "test-author")
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)
}

func TestUpdateEntry(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)

	code, data := createEntry(r, 1, "test-entry", "test-author")
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id":1, "compo":1, "author":"test-author", "title":"test-entry", "user":0, "locked":false}`, data)

	code, data = updateEntry(r, 2, gin.H{
		"compo":  1,
		"author": "test-author",
		"title":  "updated test-entry",
		"user":   0,
		"locked": false,
	})
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)

	code, data = updateEntry(r, 1, gin.H{
		"compo":  1,
		"author": "updated test-author",
		"title":  "updated test-entry",
		"user":   0,
		"locked": true,
	})
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{"id":1, "compo":1, "author":"updated test-author", "title":"updated test-entry", "user":0, "locked":true}`, data)
}

func TestDeleteEntry(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)

	code, data := createEntry(r, 1, "test-entry", "test-author")
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id":1, "compo":1, "author":"test-author", "title":"test-entry", "user":0, "locked":false}`, data)

	code, data = deleteEntry(r, 2)
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)

	code, data = deleteEntry(r, 1)
	assert.Equal(t, http.StatusNoContent, code)
}

func TestCreateUpload(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)
	createTestEntry(r, t)

	code, data := fetchAdminAPIEndpoint(r, "GET", "/admin/entries/1/uploads", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[]`, data)

	code, data = createUpload(r, 2, "test.txt", []byte("test-upload"))
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)

	code, data = createUpload(r, 1, "test.txt", []byte("test-upload"))
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"content_type":"text/plain; charset=utf-8", "entry":1, "filename":"test.txt", "id":1}`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/entries/1/uploads", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[{"content_type":"text/plain; charset=utf-8", "entry":1, "filename":"test.txt", "id":1}]`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/entries/1/uploads/1", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{"content_type":"text/plain; charset=utf-8", "entry":1, "filename":"test.txt", "id":1}`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/entries/1/uploads/1/data", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.Equal(t, `test-upload`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/entries/0/uploads/1", nil)
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/entries/0/uploads/1/data", nil)
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)
}

func TestDeleteUpload(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)
	createTestEntry(r, t)

	code, data := createUpload(r, 1, "test.txt", []byte("test-upload"))
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"content_type":"text/plain; charset=utf-8", "entry":1, "filename":"test.txt", "id":1}`, data)

	code, data = fetchAdminAPIEndpoint(r, "DELETE", "/admin/entries/0/uploads/1", nil)
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)

	code, data = fetchAdminAPIEndpoint(r, "DELETE", "/admin/entries/1/uploads/1", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{}`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/entries/1/uploads", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[]`, data)

	code, data = fetchAdminAPIEndpoint(r, "DELETE", "/admin/entries/1/uploads/1", nil)
	assert.Equal(t, http.StatusNotFound, code)
	assert.JSONEq(t, `"File not found!"`, data)
}

func TestUserAdministration(t *testing.T) {
	r := setupTestRouter(t)

	code, _ := fetchAdminAPIEndpoint(r, "GET", "/admin/users/", nil)
	assert.Equal(t, http.StatusMovedPermanently, code)

	code, data := fetchAdminAPIEndpoint(r, "GET", "/admin/users", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, "[]", data)

	code, data = fetchAdminAPIEndpoint(r, "POST", "/admin/users", bytes.NewBuffer([]byte(`{"user":"user1"}`)))
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id":1, "user":"user1"}`, data)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/users", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[{"id":1, "user":"user1"}]`, data)

	code, _ = fetchAdminAPIEndpoint(r, "GET", "/admin/users/1/", nil)
	assert.Equal(t, http.StatusMovedPermanently, code)

	code, data = fetchAdminAPIEndpoint(r, "GET", "/admin/users/1", nil)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{"id":1, "user":"user1"}`, data)

	code, data = fetchAdminAPIEndpoint(r, "PUT", "/admin/users/1", bytes.NewBuffer([]byte(`{"user":"user2"}`)))
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{"id":1, "user":"user2"}`, data)

	code, _ = fetchAdminAPIEndpoint(r, "DELETE", "/admin/users/1", bytes.NewBuffer([]byte(`{"user":"user2"}`)))
	assert.Equal(t, http.StatusNoContent, code)
}
