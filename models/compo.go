package models

import "fmt"

type Compo struct {
	ID     uint   `json:"id"`
	Name   string `json:"name" form:"name" binding:"required"`
	Locked bool   `json:"locked" form:"locked"`
	// TODO: We should output a URI to the entries for JSON.
	Entries []Entry `json:"-"`
}

func (compo Compo) AdminPath() string {
	return fmt.Sprintf("/admin/compos/%d", compo.ID)
}
