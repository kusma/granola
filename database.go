package main

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	. "granola/models"
)

func openDatabase(path string) *gorm.DB {
	db, err := gorm.Open(sqlite.Open(path), &gorm.Config{})
	if err != nil {
		panic(err)
	}

	db.AutoMigrate(&User{})
	db.AutoMigrate(&Invite{})
	db.AutoMigrate(&Compo{})
	db.AutoMigrate(&Entry{})
	db.AutoMigrate(&Upload{})

	return db
}
