package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"gorm.io/gorm"

	. "granola/models"
)

func (h RouteHandler) listUserEntries(c *gin.Context) {
	entries := []Entry{}
	user := getUser(c)
	result := h.DB.Preload("Compo").Where("user_id = ?", user.ID).Find(&entries)
	if result.Error != nil {
		panic(result.Error)
	}

	compos := []Compo{}
	result = h.DB.Where("locked = false").Find(&compos)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config":  h.Config.Routing,
		"Entries": entries,
		"Compos":  compos,
		"User":    user,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "entries",
		HTMLData: data,
		Data:     entries,
	})
}

func (h RouteHandler) newUserEntry(c *gin.Context) {
	args := struct {
		Title   string `json:"title" form:"title" binding:"required"`
		Author  string `json:"author" form:"author" binding:"required"`
		CompoID int    `json:"compo" form:"compo" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	compo := Compo{}
	result := h.DB.Where("locked = false").First(&compo, args.CompoID)
	if result.Error != nil {
		panic(fmt.Errorf("compo %d not found", args.CompoID))
	}

	entry := Entry{
		Title:  args.Title,
		Author: args.Author,
		Compo:  compo,
		UserID: getUser(c).ID,
	}

	result = h.DB.Create(&entry)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, "could not create entry")
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, entry.UserPath())
	} else {
		c.JSON(http.StatusCreated, &entry)
	}
}

func (h RouteHandler) showUserEntry(c *gin.Context) {
	user := getUser(c)
	entry := Entry{}
	result := h.DB.Preload("Uploads.Entry").Where("user_id = ?", getUser(c).ID).First(&entry, c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	compos := []Compo{}
	result = h.DB.Where("locked = false").Find(&compos)
	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": h.Config.Routing,
		"Compos": compos,
		"Entry":  entry,
		"User":   user,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "entry",
		HTMLData: data,
		Data:     entry,
	})
}

func (h RouteHandler) updateUserEntry(c *gin.Context) {
	entry := Entry{}
	result := h.DB.Preload("Uploads").Preload("Compo").Where("user_id = ? AND locked = false", getUser(c).ID).First(&entry, c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	args := struct {
		Title   string `json:"title" form:"title" binding:"required"`
		Author  string `json:"author" form:"author" binding:"required"`
		CompoID int    `json:"compo" form:"compo" binding:"required"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	compo := Compo{}
	result = h.DB.Where("locked = false").First(&compo, args.CompoID)
	if result.Error != nil {
		panic(fmt.Errorf("compo %d not found", args.CompoID))
	}

	entry.Compo = compo
	entry.Title = args.Title
	entry.Author = args.Author

	result = h.DB.Omit("Locked").Save(&entry)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, entry.UserPath())
	} else {
		c.JSON(http.StatusOK, &entry)
	}
}

func (h RouteHandler) deleteUserEntry(c *gin.Context) {
	result := h.DB.Where("user_id = ? AND locked = false", getUser(c).ID).Delete(&Entry{}, c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	if result.RowsAffected == 0 {
		panic(gorm.ErrRecordNotFound)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "/entries")
	} else {
		c.JSON(http.StatusOK, gin.H{})
	}
}

func (h RouteHandler) showRegister(c *gin.Context) {
	data := gin.H{
		"Config": h.Config.Routing,
		"Title":  "Register",
		"Invite": c.Query("invite"),
	}
	c.HTML(http.StatusOK, "register", data)
}

func (h RouteHandler) newUser(c *gin.Context) {
	args := struct {
		Username string `json:"username" form:"username" binding:"required,username"`
		Invite   string `json:"invite" form:"invite" binding:"required"`
		Password string `json:"password" form:"password" binding:"required,password"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	user, err := NewUser(h.DB, args.Username, args.Password, args.Invite)
	if err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/login")
	} else {
		c.JSON(http.StatusCreated, &user)
	}
}

func (h RouteHandler) showLogin(c *gin.Context) {
	data := gin.H{
		"Config": h.Config.Routing,
		"Title":  "Login",
	}
	c.HTML(http.StatusOK, "login", data)
}

func (h RouteHandler) loginUser(c *gin.Context) {
	session := sessions.Default(c)

	args := struct {
		Username string `json:"username" form:"username" binding:"required"`
		Password string `json:"password" form:"password" binding:"required"`
	}{}

	if err := c.ShouldBind(&args); err != nil {
		h.abortWithStatusAndText(c, http.StatusUnauthorized, err.Error())
		return
	}

	user := User{}
	h.DB.First(&user, "username = ?", args.Username)
	if !user.CheckPassword(args.Password) {
		message := "Invalid credentials"
		data := gin.H{
			"Config":   h.Config.Routing,
			"Title":    "Login",
			"Username": args.Username,
			"Error":    message,
		}
		c.Negotiate(http.StatusUnauthorized, gin.Negotiate{
			Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
			HTMLName: "login",
			HTMLData: data,
			Data:     message,
		})
		return
	}

	session.Set("user", user.ID)

	if err := session.Save(); err != nil {
		panic(err)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/")
	} else {
		c.JSON(http.StatusOK, "Successfully authenticated user")
	}
}

func getUser(c *gin.Context) User {
	tmp, _ := c.Get("user")
	return tmp.(User)
}

func (h RouteHandler) logoutUser(c *gin.Context) {
	session := sessions.Default(c)
	session.Delete("user")
	if err := session.Save(); err != nil {
		panic(err)
	}
	c.Redirect(http.StatusFound, "/")
}

func (h RouteHandler) showUserProfile(c *gin.Context) {
	user := getUser(c)
	data := gin.H{
		"Config": h.Config.Routing,
		"User":   user,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "profile",
		HTMLData: data,
		Data:     user,
	})
}

func (h RouteHandler) updateUserProfile(c *gin.Context) {
	args := struct {
		Username string `json:"username" form:"username" binding:"required"`
		Password string `json:"password" form:"password"`
	}{}
	if err := c.ShouldBind(&args); err != nil {
		panic(err)
	}

	user := getUser(c)
	user.Username = args.Username
	if args.Password != "" {
		user.SetPassword(args.Password)
	}
	result := h.DB.Save(&user)
	if result.Error != nil {
		h.abortWithStatusAndText(c, http.StatusInternalServerError, result.Error.Error())
		return
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, "/profile")
	} else {
		c.JSON(http.StatusOK, &user)
	}
}

func (h RouteHandler) newUserUpload(c *gin.Context) {
	formFile, err := c.FormFile("file")
	if err != nil {
		panic(errors.New("file not found in request"))
	}
	file, err := formFile.Open()
	if err != nil {
		panic(err)
	}

	upload := Upload{}
	upload.Filename = formFile.Filename
	upload.Content, err = ioutil.ReadAll(file)
	if err != nil {
		panic(err)
	}
	upload.ContentType = http.DetectContentType(upload.Content)

	entry := Entry{}
	result := h.DB.Preload("Uploads.Entry").Where("user_id = ?", getUser(c).ID).First(&entry, c.Param("entry_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	if entry.Locked {
		h.abortWithStatusAndText(c, http.StatusMethodNotAllowed, "Entry has been locked")
		return
	}

	upload.Entry = entry

	result = h.DB.Create(&upload)
	if result.Error != nil {
		panic(result.Error)
	}

	// It would have been preferrable to have a reference from Upload to Entry
	// so we could do upload.Entry.CompoID to find the CompoID for the URL, but
	// that somehow made all fields within Upload.Entry required when
	// data-binding the new Upload.
	result = h.DB.First(&upload.Entry, upload.EntryID)
	if result.Error != nil {
		panic(result.Error)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusFound, upload.UserPath())
	} else {
		c.JSON(http.StatusCreated, &upload)
	}
}

func (h RouteHandler) showUserUpload(c *gin.Context) {
	user := getUser(c)
	upload := Upload{}
	result := h.DB.Omit("Content").
		Preload("Entry").
		Preload("Entry.Compo").
		Joins("JOIN entries on entries.id = uploads.entry_id").
		Where("entries.user_id = ?", getUser(c).ID).
		First(&upload, c.Param("upload_id"))

	if result.Error != nil {
		panic(result.Error)
	}

	data := gin.H{
		"Config": h.Config.Routing,
		"Upload": upload,
		"User":   user,
	}
	c.Negotiate(http.StatusOK, gin.Negotiate{
		Offered:  []string{gin.MIMEJSON, gin.MIMEHTML},
		HTMLName: "upload",
		HTMLData: data,
		Data:     upload,
	})
}

func (h RouteHandler) getUserUpload(c *gin.Context) {
	upload := Upload{}
	result := h.DB.Joins("JOIN entries on entries.id = uploads.entry_id").Where("entries.user_id = ?", getUser(c).ID).First(&upload, c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}
	c.Header("Content-Disposition", "attachment; filename="+upload.Filename)
	c.Header("Content-Type", "application/octet-stream")
	c.Data(http.StatusOK, upload.ContentType, upload.Content)
}

func (h RouteHandler) deleteUserUpload(c *gin.Context) {
	upload := Upload{}
	result := h.DB.Joins("JOIN entries on entries.id = uploads.entry_id").
		Where("entries.user_id = ?", getUser(c).ID).
		First(&upload, c.Param("upload_id"))
	if result.Error != nil {
		panic(result.Error)
	}

	result = h.DB.Delete(upload)
	if result.Error != nil {
		panic(result.Error)
	}

	format := c.NegotiateFormat(gin.MIMEJSON, gin.MIMEHTML)
	if format == gin.MIMEHTML {
		c.Redirect(http.StatusSeeOther, "..")
	} else {
		c.JSON(http.StatusOK, gin.H{})
	}
}

func (h RouteHandler) registerPrivateRoutes(root *gin.RouterGroup) {
	private := root.Group("/")
	private.Use(h.requireAuth)
	private.POST("/logout", h.logoutUser)

	profile := private.Group("/profile")
	profile.GET("", h.showUserProfile)
	profile.PUT("", h.updateUserProfile)

	entries := private.Group("/entries")
	entries.GET("", h.listUserEntries)
	entries.POST("", h.newUserEntry)

	entry := entries.Group("/:entry_id")
	entry.GET("", h.showUserEntry)
	entry.PUT("", h.updateUserEntry)
	entry.DELETE("", h.deleteUserEntry)

	uploads := entry.Group("/uploads")
	uploads.POST("", h.newUserUpload)

	upload := uploads.Group("/:upload_id")
	upload.GET("", h.showUserUpload)
	upload.DELETE("", h.deleteUserUpload)
	upload.GET("/data", h.getUserUpload)
}

func (h RouteHandler) registerUserRoutes(r *gin.Engine) {
	root := r.Group("/")
	root.GET("", h.showHome)
	root.GET("tribute", func(c *gin.Context) {
		c.Redirect(http.StatusPermanentRedirect, "http://www.slengpung.com/?id=7623")
	})

	register := root.Group("/register")
	register.GET("", h.showRegister)
	register.POST("", h.newUser)

	login := root.Group("/login")
	login.GET("", h.showLogin)
	login.POST("", h.loginUser)

	h.registerPrivateRoutes(root)
}
