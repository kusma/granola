package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

func getHTML(r *gin.Engine, endpoint string) (int, string) {
	req, _ := http.NewRequest("GET", endpoint, nil)
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	responseData, _ := ioutil.ReadAll(w.Body)
	return w.Code, string(responseData)
}

func uploadFileWithCookies(r *gin.Engine, method string, endpoint string, data []byte, filename string, cookies []string) (int, string, []string) {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	contentType := writer.FormDataContentType()
	part, _ := writer.CreateFormFile("file", filename)
	part.Write(data)
	writer.Close()
	return requestApiWithCookies(r, method, endpoint, contentType, body, cookies)
}

func requestJsonApiWithCookies(r *gin.Engine, method string, endpoint string, data []byte, cookies []string) (int, string, []string) {
	body := bytes.NewBuffer(data)
	contentType := "application/json; charset=UTF-8"
	return requestApiWithCookies(r, method, endpoint, contentType, body, cookies)
}

func requestApiWithCookies(r *gin.Engine, method string, endpoint string, contentType string, body io.Reader, cookies []string) (int, string, []string) {
	req, _ := http.NewRequest(method, endpoint, body)
	req.Header.Set("Content-Type", contentType)

	for _, cookie := range cookies {
		req.Header.Set("Cookie", cookie)
	}

	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	return w.Code, string(responseData), w.Header().Values("Set-Cookie")
}

func fetchAPIEndpoint(r *gin.Engine, method string, endpoint string, data []byte) (int, string) {
	code, response, _ := requestJsonApiWithCookies(r, method, endpoint, data, []string{})
	return code, response
}

func setupTestRouter(t *testing.T) *gin.Engine {
	config := Config{
		Secret:       []byte("not-very-secret"),
		DatabasePath: ":memory:",
		Admins:       map[string]string{"admin": "password"},
	}
	config.Routing.MinimumUsernameLength = 3
	config.Routing.MinimumPasswordLength = 5
	s := Server{}.Bootstrap(config)

	// concurrent database-access for in-memory databases doesn't work well
	// in sqlite, so let's limit the open connections.
	db := s.RouteHandler.DB
	sql, _ := db.DB()
	sql.SetMaxOpenConns(1)

	t.Cleanup(func() {
		sql, _ := db.DB()
		sql.Close()
		db = nil
	})

	return s.Engine
}

func registerUser(r *gin.Engine, username string, invite string, password string) (int, string) {
	str, _ := json.Marshal(gin.H{
		"username": username,
		"invite":   invite,
		"password": password,
	})
	return fetchAPIEndpoint(r, "POST", "/register", []byte(str))
}

func loginUser(r *gin.Engine, username string, password string) (int, string, []string) {
	str, _ := json.Marshal(gin.H{
		"username": username,
		"password": password,
	})
	return requestJsonApiWithCookies(r, "POST", "/login", []byte(str), []string{})
}

func createUserEntry(r *gin.Engine, compo int, title string, author string, cookies []string) (int, string) {
	str, _ := json.Marshal(gin.H{
		"compo":  compo,
		"title":  title,
		"author": author,
	})
	code, data, _ := requestJsonApiWithCookies(r, "POST", "/entries", []byte(str), cookies)
	return code, data
}

func createUserTestEntry(r *gin.Engine, t *testing.T, cookies []string) {
	code, data := createUserEntry(r, 1, "test-entry", "test-author", cookies)
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"author":"test-author", "compo":1, "id":1, "title":"test-entry", "user":1, "locked":false}`, data)
}

func updateUserEntry(r *gin.Engine, entry int, compo int, title string, author string, cookies []string) (int, string) {
	str, _ := json.Marshal(gin.H{
		"compo":  compo,
		"title":  title,
		"author": author,
	})
	code, data, _ := requestJsonApiWithCookies(r, "PUT", fmt.Sprintf("/entries/%d", entry), []byte(str), cookies)
	return code, data
}

func fetchAdminAPIEndpoint(r *gin.Engine, method string, endpoint string, data io.Reader) (int, string) {

	req, _ := http.NewRequest(method, endpoint, data)
	req.SetBasicAuth("admin", "password")
	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	return w.Code, string(responseData)
}

func createCompo(r *gin.Engine, name string, locked bool) (int, string) {
	str, _ := json.Marshal(gin.H{"name": name, "locked": locked})
	return fetchAdminAPIEndpoint(r, "POST", "/admin/compos", bytes.NewBuffer(str))
}

func createTestCompo(r *gin.Engine, t *testing.T) {
	code, data := createCompo(r, "test compo", false)
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id":1, "name":"test compo", "locked":false}`, data)
}

func updateCompo(r *gin.Engine, compo int, obj gin.H) (int, string) {
	str, _ := json.Marshal(obj)
	return fetchAdminAPIEndpoint(r, "PUT", fmt.Sprintf("/admin/compos/%d", compo), bytes.NewBuffer([]byte(str)))
}

func adminCreateInvite(r *gin.Engine, key string) (int, string) {
	str, _ := json.Marshal(gin.H{"key": key})
	return fetchAdminAPIEndpoint(r, "POST", "/admin/invites", bytes.NewBuffer(str))
}

func createInvite(r *gin.Engine, t *testing.T, key string) {
	code, _ := adminCreateInvite(r, key)
	assert.Equal(t, http.StatusCreated, code)
}

func lockEntry(r *gin.Engine, entry int) (int, string) {
	obj, err := getEntry(r, entry)
	if err != nil {
		panic(err)
	}

	obj["locked"] = true
	return updateEntry(r, entry, obj)
}

func createEntry(r *gin.Engine, compo int, title string, author string) (int, string) {
	str, _ := json.Marshal(gin.H{
		"title":  title,
		"author": author,
	})
	return fetchAdminAPIEndpoint(r, "POST", fmt.Sprintf("/admin/compos/%d/entries", compo), bytes.NewBuffer([]byte(str)))
}

func adminCreateTestEntry(r *gin.Engine, t *testing.T) {
	code, data := createEntry(r, 1, "test-entry", "test-author")
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id":1, "compo":1, "author":"test-author", "title":"test-entry", "user":0, "locked":false}`, data)
}

func createTestEntry(r *gin.Engine, t *testing.T) {
	code, data := createEntry(r, 1, "test-entry", "test-author")
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id":1, "compo":1, "author":"test-author", "title":"test-entry", "user":0, "locked":false}`, data)
}

func getEntry(r *gin.Engine, entry int) (gin.H, error) {
	code, data := fetchAdminAPIEndpoint(r, "GET", fmt.Sprintf("/admin/entries/%d", entry), nil)
	if code != http.StatusOK {
		return nil, fmt.Errorf("got HTTP error %d, message: %v", code, data)
	}
	obj := gin.H{}
	if err := json.Unmarshal([]byte(data), &obj); err != nil {
		return nil, err
	}
	return obj, nil
}

func updateEntry(r *gin.Engine, entry int, obj gin.H) (int, string) {
	str, _ := json.Marshal(obj)
	return fetchAdminAPIEndpoint(r, "PUT", fmt.Sprintf("/admin/entries/%d", entry), bytes.NewBuffer([]byte(str)))
}

func deleteEntry(r *gin.Engine, entry int) (int, string) {
	return fetchAdminAPIEndpoint(r, "DELETE", fmt.Sprintf("/admin/entries/%d", entry), nil)
}

func createUpload(r *gin.Engine, entry int, filename string, data []byte) (int, string) {
	body := new(bytes.Buffer)
	writer := multipart.NewWriter(body)
	part, _ := writer.CreateFormFile("file", filename)
	part.Write(data)
	writer.Close()
	endpoint := fmt.Sprintf("/admin/entries/%d/uploads", entry)

	req, _ := http.NewRequest("POST", endpoint, body)
	req.SetBasicAuth("admin", "password")
	req.Header.Set("Content-Type", writer.FormDataContentType())
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)

	responseData, _ := ioutil.ReadAll(w.Body)
	return w.Code, string(responseData)
}
