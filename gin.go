package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"

	method "github.com/bu/gin-method-override"
	"github.com/gin-contrib/multitemplate"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/locales/en"
	ut "github.com/go-playground/universal-translator"
	"github.com/go-playground/validator/v10"
	translations "github.com/go-playground/validator/v10/translations/en"
)

type Validator struct {
	Func    validator.Func
	Message string
}

func createRender() multitemplate.Renderer {
	r := multitemplate.NewRenderer()
	addTemplate := func(templateName string, templatePath string) {
		baseTemplate := "templates/base.html"
		r.AddFromFilesFuncs(templateName, template.FuncMap{
			"templateName": func() string { return templateName },
		}, baseTemplate, templatePath)
	}
	addTemplate("home", "templates/home.html")
	addTemplate("register", "templates/register.html")
	addTemplate("login", "templates/login.html")
	addTemplate("profile", "templates/profile.html")
	addTemplate("entries", "templates/entries.html")
	addTemplate("entry", "templates/entry.html")
	addTemplate("upload", "templates/upload.html")
	addTemplate("error", "templates/error.html")

	addTemplate("admin", "templates/admin/admin.html")
	addTemplate("admin/invites", "templates/admin/invites.html")
	addTemplate("admin/user", "templates/admin/user.html")
	addTemplate("admin/users", "templates/admin/users.html")
	addTemplate("admin/entry", "templates/admin/entry.html")
	addTemplate("admin/entries", "templates/admin/entries.html")
	addTemplate("admin/compo", "templates/admin/compo.html")
	addTemplate("admin/compos", "templates/admin/compos.html")
	addTemplate("admin/upload", "templates/admin/upload.html")
	addTemplate("admin/uploads", "templates/admin/uploads.html")
	return r
}

func configureStaticFiles(r *gin.Engine) {
	files, err := ioutil.ReadDir("./static")
	if err != nil {
		panic(err)
	}

	for _, f := range files {
		if f.IsDir() {
			r.Static("/"+f.Name(), "./static/"+f.Name())
		} else {
			r.StaticFile("/"+f.Name(), "./static/"+f.Name())
		}
	}
}

func configureValidation(validators map[string]Validator) ut.Translator {
	if validation, ok := binding.Validator.Engine().(*validator.Validate); ok {
		englishLocaleTranslator := en.New()
		universalTranslator := ut.New(englishLocaleTranslator, englishLocaleTranslator)
		// this is usually known or extracted from http 'Accept-Language' header
		// also see uni.FindTranslator(...)
		translator, found := universalTranslator.GetTranslator("en")
		if !found {
			log.Fatal("'en' translator not found")
		}
		translations.RegisterDefaultTranslations(validation, translator)

		for tag, v := range validators {
			f := v.Func
			msg := v.Message

			if err := validation.RegisterValidation(tag, f); err != nil {
				log.Fatal(fmt.Printf("Could not register validator for '%s'.", tag))
			}

			if msg == "" {
				continue
			}

			registerFn := func(ut ut.Translator) error {
				return ut.Add(tag, msg, true)
			}

			translationFn := func(ut ut.Translator, fe validator.FieldError) string {
				return msg
			}

			validation.RegisterTranslation(tag, translator, registerFn, translationFn)
		}

		return translator
	} else {
		log.Fatal("Could not configure Gin validator")
	}

	return nil
}

func configureGin(secret []byte, validators map[string]Validator) (*gin.Engine, ut.Translator) {
	r := gin.Default()

	store := cookie.NewStore(secret)
	r.Use(sessions.Sessions("session", store))
	r.Use(method.ProcessMethodOverride(r))

	r.MaxMultipartMemory = 128 << 20 // 128 MiB
	r.HTMLRender = createRender()

	configureStaticFiles(r)
	t := configureValidation(validators)

	return r, t
}
