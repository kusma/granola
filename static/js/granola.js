(function() {
  document.addEventListener('DOMContentLoaded', function() {
    document.querySelectorAll('.deletable form').forEach(function(form) {
      form.addEventListener('submit', function(event) {
        event.preventDefault();

        if (confirm("Are you sure you want to delete this?")) {
          fetch(form.action, { method: 'DELETE' }).then((response) => {
            if (response.ok) {
              let p = form;

              while (p = p.parentNode) {
                if (p.classList && !p.classList.contains('deletable')) {
                  continue;
                }

                p.classList.add('deleting');
                return setTimeout(() =>
                {
                  p.remove();
                  if (!document.querySelectorAll('.deletable').length) {
                    document.querySelector('#content').classList.add('empty');
                  }
                  return Promise.resolve();
                }, 1000);
              }
            } else {
              return Promise.reject(response);
            }
          });
        }
      });
    });

    const password = document.getElementById('password');
    const passwordConfirm = document.getElementById('password-confirm');
    const passwordConfirmWrapper = document.getElementById('password-confirm-wrapper');

    if (password && passwordConfirm && passwordConfirmWrapper) {
      const validatePassword = () => {
        const customValidity = password.value == passwordConfirm.value
          ? ''
          : 'Passwords don\'t match';

        passwordConfirm.setCustomValidity(customValidity);
      };

      password.addEventListener('change', validatePassword);
      passwordConfirm.addEventListener('keyup', validatePassword);
      passwordConfirmWrapper.style.display = 'block';
    }

    document.querySelectorAll('.clipboard-copy').forEach(function(element) {
      element.addEventListener('click', function(event) {
        navigator.clipboard.writeText(element.innerText);
      });
    });
  });
})();
