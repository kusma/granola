package main

import (
	"fmt"
	"net/http"
	"sync"
	"sync/atomic"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestRegisterValidation(t *testing.T) {
	r := setupTestRouter(t)

	code, data := registerUser(r, "", "test-invite", "test-password")
	assert.Equal(t, http.StatusBadRequest, code)
	assert.Contains(t, data, "Username is a required field")

	code, data = registerUser(r, "test-user", "", "test-password")
	assert.Equal(t, http.StatusBadRequest, code)
	assert.Contains(t, data, "Invite is a required field")

	code, data = registerUser(r, "test-user", "test-invite", "")
	assert.Equal(t, http.StatusBadRequest, code)
	assert.Contains(t, data, "Password is a required field")

	code, data = registerUser(r, "u", "test-invite", "test-password")
	assert.Equal(t, http.StatusBadRequest, code)
	assert.Contains(t, data, "Username must be at least 3 characters long")

	code, data = registerUser(r, "test-user", "test-invite", "p")
	assert.Equal(t, http.StatusBadRequest, code)
	assert.Contains(t, data, "Password must be at least 5 characters long")

	code, data = registerUser(r, "test-user", "invalid-invitation", "test-password")
	assert.Equal(t, http.StatusBadRequest, code)
	assert.Contains(t, data, "invalid invitation")
}

func TestRegister(t *testing.T) {
	r := setupTestRouter(t)

	createInvite(r, t, "test-invite")
	createInvite(r, t, "test-invite-2")

	code, data := registerUser(r, "test-user", "test-invite", "test-password")
	assert.JSONEq(t, `{"id" : 1, "user": "test-user"}`, data)
	assert.Equal(t, http.StatusCreated, code)

	// Should fail because `test-user` is already registered.
	code, _ = registerUser(r, "test-user", "test-invite-2", "test-password")
	assert.Equal(t, http.StatusBadRequest, code)

	// Should succeed because `test-user-2` is not registered.
	code, data = registerUser(r, "test-user-2", "test-invite-2", "test-password")
	assert.JSONEq(t, `{"id" : 2, "user": "test-user-2"}`, data)
	assert.Equal(t, http.StatusCreated, code)
}

func TestRegisterSpam(t *testing.T) {
	r := setupTestRouter(t)
	createInvite(r, t, "test-invite")

	N := 1000

	if testing.Short() {
		N = 100
	}

	var wg sync.WaitGroup

	var succeeded uint64

	wg.Add(N)
	for i := 0; i < N; i++ {
		i := i
		go func() {
			defer wg.Done()

			user := fmt.Sprintf("user-%d", i)
			code, data := registerUser(r, user, "test-invite", "test-password")

			if code == http.StatusCreated {
				atomic.AddUint64(&succeeded, 1)
			} else {
				assert.Equal(t, http.StatusBadRequest, code)
				assert.Contains(t, data, "invalid invitation")
			}
		}()
	}
	wg.Wait()
	assert.Equal(t, uint64(1), succeeded)
}

func TestLogin(t *testing.T) {
	r := setupTestRouter(t)

	code, data, _ := loginUser(r, "test-user", "test-password")
	assert.Equal(t, http.StatusUnauthorized, code)
	assert.JSONEq(t, `"Invalid credentials"`, data)

	createInvite(r, t, "test-invite")
	code, data = registerUser(r, "test-user", "test-invite", "test-password")
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id" : 1, "user" : "test-user"}`, data)

	code, data, _ = loginUser(r, "test-user", "invalid-password")
	assert.Equal(t, http.StatusUnauthorized, code)
	assert.JSONEq(t, `"Invalid credentials"`, data)

	var cookies []string
	code, data, cookies = loginUser(r, "test-user", "test-password")
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `"Successfully authenticated user"`, data)

	_, data, _ = requestJsonApiWithCookies(r, "GET", "/", nil, cookies)
	assert.Contains(t, data, `Welcome, <a href="/profile">test-user</a>`)

	code, _, cookies = requestJsonApiWithCookies(r, "POST", "/logout", nil, cookies)
	assert.Equal(t, http.StatusFound, code)

	_, data, _ = requestJsonApiWithCookies(r, "GET", "/", nil, cookies)
	assert.NotContains(t, data, `Logged in as "test-user"`)
	assert.Contains(t, data, `see yourself inside`)
}

func TestUserEditProfile(t *testing.T) {
	r := setupTestRouter(t)
	createInvite(r, t, "test-invite")
	registerUser(r, "test-user", "test-invite", "test-password")
	_, _, cookies := loginUser(r, "test-user", "test-password")

	code, _, _ := requestJsonApiWithCookies(r, "GET", "/profile", nil, cookies)
	assert.Equal(t, http.StatusOK, code)

	code, _, _ = requestJsonApiWithCookies(r, "PUT", "/profile", []byte(`{"username":"test-user2","password":"test-password2"}`), cookies)
	assert.Equal(t, http.StatusOK, code)

	requestJsonApiWithCookies(r, "POST", "/logout", nil, cookies)

	code, data, cookies := loginUser(r, "test-user2", "test-password2")
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `"Successfully authenticated user"`, data)

	_, data, _ = requestJsonApiWithCookies(r, "GET", "/", nil, cookies)
	assert.Contains(t, data, `Welcome, <a href="/profile">test-user2</a>`)
}

func TestInviteForwarding(t *testing.T) {
	r := setupTestRouter(t)

	code, _ := fetchAPIEndpoint(r, "GET", "/register/", nil)
	assert.Equal(t, http.StatusMovedPermanently, code)

	code, data := getHTML(r, "/register?invite=test-invite")
	assert.Equal(t, http.StatusOK, code)
	assert.Contains(t, data, `value="test-invite"`)
}

func TestUserCreateEntry(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)

	code, data := createCompo(r, "test compo 2", true)
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id":2, "name":"test compo 2", "locked":true}`, data)

	createInvite(r, t, "test-invite")
	registerUser(r, "test-user", "test-invite", "test-password")

	_, _, cookies := loginUser(r, "test-user", "test-password")

	code, data, _ = requestJsonApiWithCookies(r, "GET", "/entries", nil, cookies)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[]`, data)

	code, data = createUserEntry(r, 0, "test-entry", "test-author", cookies)
	assert.Equal(t, http.StatusBadRequest, code)
	assert.Contains(t, data, "CompoID is a required field")

	code, data = createUserEntry(r, 1, "entry 1", "author 1", cookies)
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"author":"author 1", "compo":1, "id":1, "title":"entry 1", "user":1, "locked":false}`, data)

	code, data, _ = requestJsonApiWithCookies(r, "GET", "/entries", nil, cookies)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[{"author":"author 1", "compo":1, "id":1, "title":"entry 1", "user":1, "locked":false}]`, data)

	code, data, _ = requestJsonApiWithCookies(r, "GET", "/entries/1", nil, cookies)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{"author":"author 1", "compo":1, "id":1, "title":"entry 1", "user":1, "locked":false}`, data)

	code, data = createUserEntry(r, 2, "entry 2", "author 2", cookies)
	assert.Equal(t, http.StatusBadRequest, code)
	assert.JSONEq(t, `"compo 2 not found"`, data)
}

func TestUserUpdateEntry(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)

	code, data := createCompo(r, "test compo 2", true)
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id":2, "name":"test compo 2", "locked":true}`, data)

	createInvite(r, t, "test-invite")
	registerUser(r, "test-user", "test-invite", "test-password")

	_, _, cookies := loginUser(r, "test-user", "test-password")

	code, data = createUserEntry(r, 1, "entry 1", "author 1", cookies)
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"author":"author 1", "compo":1, "id":1, "title":"entry 1", "user":1, "locked":false}`, data)

	code, data = updateUserEntry(r, 1, 1, "updated entry 1", "updated author 1", cookies)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{"author":"updated author 1", "compo":1, "id":1, "title":"updated entry 1", "user":1, "locked":false}`, data)

	code, data = updateUserEntry(r, 1, 2, "updated entry 1", "updated author 1", cookies)
	assert.Equal(t, http.StatusBadRequest, code)
	assert.JSONEq(t, `"compo 2 not found"`, data)
}

func TestUserDeleteEntry(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)
	createInvite(r, t, "test-invite")
	registerUser(r, "test-user", "test-invite", "test-password")
	_, _, cookies := loginUser(r, "test-user", "test-password")

	createUserTestEntry(r, t, cookies)

	code, data, _ := requestJsonApiWithCookies(r, "GET", "/entries", nil, cookies)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[{"author":"test-author", "compo":1, "id":1, "title":"test-entry", "user":1, "locked":false}]`, data)

	code, _, _ = requestJsonApiWithCookies(r, "DELETE", "/entries/1", nil, cookies)
	assert.Equal(t, http.StatusOK, code)

	code, data, _ = requestJsonApiWithCookies(r, "GET", "/entries", nil, cookies)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `[]`, data)

	code, data, _ = requestJsonApiWithCookies(r, "DELETE", "/entries/1", nil, cookies)
	assert.Equal(t, http.StatusNotFound, code)
	assert.Contains(t, data, `"File not found!"`)
}

func TestUserCreateUpload(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)
	createInvite(r, t, "test-invite")
	registerUser(r, "test-user", "test-invite", "test-password")
	_, _, cookies := loginUser(r, "test-user", "test-password")
	createUserTestEntry(r, t, cookies)

	code, data, _ := requestJsonApiWithCookies(r, "POST", "/entries", []byte(`{"compo":1, "title":"entry 2", "author":"author 2"}`), cookies)
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"id": 2, "compo":1, "title":"entry 2", "author":"author 2", "user":1, "locked": false}`, data)

	code, data = lockEntry(r, 2)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{"id": 2, "compo":1, "title":"entry 2", "author":"author 2", "user":1, "locked": true}`, data)

	code, data, _ = uploadFileWithCookies(r, "POST", "/entries/1/uploads", []byte{1, 2, 3}, "file.bin", cookies)
	assert.Equal(t, http.StatusCreated, code)
	assert.JSONEq(t, `{"content_type":"application/octet-stream", "entry":1, "filename":"file.bin", "id":1}`, data)

	code, data, _ = requestJsonApiWithCookies(r, "GET", "/entries/1/uploads/1", nil, cookies)
	assert.Equal(t, http.StatusOK, code)
	assert.JSONEq(t, `{"content_type":"application/octet-stream", "entry":1, "filename":"file.bin", "id":1}`, data)

	code, data, _ = requestJsonApiWithCookies(r, "GET", "/entries/1/uploads/1/data", nil, cookies)
	assert.Equal(t, http.StatusOK, code)
	assert.Equal(t, string("\x01\x02\x03"), data)

	code, _ = fetchAPIEndpoint(r, "GET", "/entries/1/", nil)
	assert.Equal(t, http.StatusMovedPermanently, code)
	code, _ = fetchAPIEndpoint(r, "GET", "/entries/1/uploads/1/", nil)
	assert.Equal(t, http.StatusMovedPermanently, code)
	code, _ = fetchAPIEndpoint(r, "GET", "/entries/1/uploads/1/data/", nil)
	assert.Equal(t, http.StatusMovedPermanently, code)

	code, data, _ = uploadFileWithCookies(r, "POST", "/entries/2/uploads", []byte{1, 2, 3}, "file.bin", cookies)
	assert.Equal(t, http.StatusMethodNotAllowed, code)
	assert.JSONEq(t, `"Entry has been locked"`, data)

	code, _, _ = requestJsonApiWithCookies(r, "DELETE", "/entries/1/uploads/1", nil, cookies)
	assert.Equal(t, http.StatusOK, code)
}

func TestUserDeleteUpload(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)
	createInvite(r, t, "test-invite")
	registerUser(r, "test-user", "test-invite", "test-password")
	_, _, cookies := loginUser(r, "test-user", "test-password")
	createUserTestEntry(r, t, cookies)
	uploadFileWithCookies(r, "POST", "/entries/1/uploads", []byte{1, 2, 3}, "file.bin", cookies)

	code, _, _ := requestJsonApiWithCookies(r, "DELETE", "/entries/1/uploads/1", nil, cookies)
	assert.Equal(t, http.StatusOK, code)

	code, data, _ := requestJsonApiWithCookies(r, "DELETE", "/entries/1/uploads/1", nil, cookies)
	assert.Equal(t, http.StatusNotFound, code)
	assert.Contains(t, data, `"File not found!"`)
}

func TestXUserAccess(t *testing.T) {
	r := setupTestRouter(t)

	createTestCompo(r, t)
	createInvite(r, t, "test-invite1")
	createInvite(r, t, "test-invite2")
	registerUser(r, "test-user1", "test-invite1", "test-password1")
	registerUser(r, "test-user2", "test-invite2", "test-password2")
	_, _, cookies := loginUser(r, "test-user1", "test-password1")
	createUserTestEntry(r, t, cookies)
	uploadFileWithCookies(r, "POST", "/entries/1/uploads", []byte{1, 2, 3}, "file.bin", cookies)

	_, _, cookies = loginUser(r, "test-user2", "test-password2")

	code, data, _ := requestJsonApiWithCookies(r, "GET", "/entries/1", nil, cookies)
	assert.Equal(t, http.StatusNotFound, code)
	assert.Contains(t, data, `"File not found!"`)

	code, data, _ = requestJsonApiWithCookies(r, "DELETE", "/entries/1", nil, cookies)
	assert.Equal(t, http.StatusNotFound, code)
	assert.Contains(t, data, `"File not found!"`)

	code, data, _ = requestJsonApiWithCookies(r, "GET", "/entries/1/uploads/1", nil, cookies)
	assert.Equal(t, http.StatusNotFound, code)
	assert.Contains(t, data, `"File not found!"`)

	code, data, _ = uploadFileWithCookies(r, "POST", "/entries/1/uploads", []byte{1, 2, 3}, "file.bin", cookies)
	assert.Equal(t, http.StatusNotFound, code)
	assert.Contains(t, data, `"File not found!"`)

	code, data, _ = requestJsonApiWithCookies(r, "GET", "/entries/1/uploads/1/data", nil, cookies)
	assert.Equal(t, http.StatusNotFound, code)
	assert.Contains(t, data, `"File not found!"`)

	code, data, _ = requestJsonApiWithCookies(r, "DELETE", "/entries/1/uploads/1", nil, cookies)
	assert.Equal(t, http.StatusNotFound, code)
	assert.Contains(t, data, `"File not found!"`)

	code, data = updateUserEntry(r, 1, 1, "updated entry 1", "updated author 1", cookies)
	assert.Equal(t, http.StatusNotFound, code)
	assert.Contains(t, data, `"File not found!"`)
}
