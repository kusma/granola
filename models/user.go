package models

import (
	"fmt"
	"errors"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	ID           uint   `json:"id"`
	Username     string `json:"user" form:"username" binding:"required" gorm:"default:null;not null;uniqueIndex:idx_uq_users_username"`
	PasswordHash string `json:"-"`
}

func NewUser(db *gorm.DB, username string, password string, invite string) (User, error) {
	user := User{Username: username}
	if err := user.SetPassword(password); err != nil {
		return user, err
	}

	tx := db.Begin()
	if err := tx.Create(&user).Error; err != nil {
		tx.Rollback()
		return user, err
	}

	result := tx.Model(&Invite{}).Where("key = ? AND user_id is null", invite).Update("UserID", user.ID)
	if result.Error != nil || result.RowsAffected == 0 {
		tx.Rollback()
		return user, errors.New("invalid invitation")
	}

	if err := tx.Commit().Error; err != nil {
		return user, err
	}

	return user, nil
}

func (user User) AdminPath() string {
	return fmt.Sprintf("/admin/users/%d", user.ID)
}

func (user *User) SetPassword(plainTextPassword string) error {
	password := []byte(plainTextPassword)

	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.DefaultCost)
	if err != nil {
		return err
	}

	user.PasswordHash = string(hashedPassword)

	return nil
}

func (user *User) CheckPassword(plainTextPassword string) bool {
	if user.PasswordHash == "" {
		return false
	}

	password := []byte(plainTextPassword)
	hashedPassword := []byte(user.PasswordHash)
	err := bcrypt.CompareHashAndPassword(hashedPassword, password)
	return err == nil
}
