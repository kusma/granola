package models

import "database/sql"

type Invite struct {
	ID     uint          `json:"-"`
	Key    string        `json:"key" form:"key" gorm:"uniqueIndex:idx_uq_invites_key"`
	UserID sql.NullInt64 `json:"user" gorm:"uniqueIndex:idx_uq_invites_userid"`
	User   User          `json:"-" binding:"-"` // Gin should ignore User; it should only be populated by Gorm.
}
